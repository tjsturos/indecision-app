import { Action } from '../public/Components/Action';
import { AddOption } from '../public/Components/AddOption';
import { Header } from '../public/Components/Header';
import { Options } from '../public/Components/Options';

class IndecisionApp extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            options : []
        }
        this.handleAddOption = this.handleAddOption.bind(this);
        this.handleRemoveAll = this.handleRemoveAll.bind(this);
        this.onMakeDecision = this.onMakeDecision.bind(this);
        this.handleDeleteOption = this.handleDeleteOption.bind(this);
    }

    handleDeleteOption(optionToRemove) {
        this.setState((prevState) => ({ options : prevState.options.filter((option  => option !== optionToRemove))}))
    }

    handleAddOption(option) {
        if(!option) {
            return 'Enter valid value to add item';
        } else if (this.state.options.indexOf(option) > -1) {
            return 'This option already exists';
        }
            this.setState((previousState) => ({ options : [...previousState.options, option] }));
    }

    handleRemoveAll() {
        this.setState(() => ({ options : [] }));
    }

    onMakeDecision(e) {
        const options = this.state.options;
        const max = options.length;
        const randomNum = Math.floor(Math.random() * max);
        const option = options[randomNum];
        alert(
            option
        );
    }

    componentDidMount() {
        try {    
            const json = localStorage.getItem('options');
            
            if(json) {
                const options = JSON.parse(json);
                this.setState(() => ({options}));
            }

        } catch (e) {
            // do nothing at all
        }
      
        
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.options.length !== this.state.options.length) {
            console.log('saving data');            
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }

    render() {
        const subtitle = "Put your life in the hands of a computer.";
        const options = this.state.options;

       return (
        <div>
            <Header subtitle={subtitle} />
            <Action disabled={options.length > 0} action={this.onMakeDecision}/>
            <Options options={options} handleDeleteOption={this.handleDeleteOption} handleDeleteOptions={this.handleRemoveAll}/>
            <AddOption action={this.handleAddOption}/>
        </div>
       ) 
    }
}


// const User = (props) => {
//     return (
//         <div>
//             <p>Name: {props.name}</p>
//             <p>Age: {props.age}</p>
//         </div>
//     )
// };

ReactDOM.render(<IndecisionApp />, document.getElementById('app'))