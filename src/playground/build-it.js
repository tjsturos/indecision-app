const appRoot = document.getElementById('app');

class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <h3>{this.props.subTitle}</h3>
            </div>
        )
    }
}
class Details extends React.Component {
    render() {
        return (
        <div>
            <h4>{this.props.text}</h4>
            <p>{`By the way... you've toggled this ${this.props.toggleCount} times...`}</p>
        </div>
        );
    }
}

class ToggleButton extends React.Component {
    render() {
        return (
            <div>
            <button onClick={this.props.action}>{!this.props.isVisible ? "Show Details" : "Hide Details"}</button>            
            </div>
        )
    }
}

class VisibilityToggle extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            isVisible : false,
            toggleCount : props.toggleCount
        }
        this.toggleVisibility = this.toggleVisibility.bind(this)
    }

    toggleVisibility() {
        this.setState((prevState) => {
            return {
                isVisible : !prevState.isVisible,
                toggleCount : !prevState.isVisible ? prevState.toggleCount + 1 : prevState.toggleCount
            }
        })
    }

    render() {
        return (
            <div>
                <Header title={this.props.title} subTitle={this.props.subtitle} />
                <ToggleButton isVisible={this.state.isVisible} action={this.toggleVisibility} />
                
                {this.state.isVisible && <Details text={this.props.details} toggleCount={this.state.toggleCount} />}
            </div>
        );
    }
    
}

VisibilityToggle.defaultProps = {
    title: "Visibility 'Build-It'",
    subtitle: "Building \"Extreme\" Challenges. See details below.",
    toggleCount : 0,
    details: "Some super important details that you wish you had read 3 seconds sooner!"
}

ReactDOM.render(<VisibilityToggle />, appRoot);


