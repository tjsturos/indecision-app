import React from 'react';
const Action = (props) => (
    <div>
        <button className="big-button" disabled={!props.disabled} onClick={props.action}>What should I do?</button>
    </div>
);

export default Action;