import React from 'react';

import Option from './Option';

const Options = (props) => (
    <div>
        <div className="widget-header">
            <h3 className="widget-header__title">Your Options</h3>
            <button className="button button--link" onClick={props.handleDeleteOptions}>Remove All</button>
        </div>
    
        
        {
           props.options.length > 0 ? props.options.map((option, index) => <Option handleDeleteOption={props.handleDeleteOption} count={index + 1} key={index} text={option} />) : <p className="widget__message">Need to add Options</p>
        }
    </div>
)

export default Options;