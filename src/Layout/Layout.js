import React from 'react';

import Action from '../Components/Action';
import AddOption from '../Components/AddOption';
import Header from '../Components/Header';
import OptionModal from '../Components/OptionModal';
import Options from '../Components/Options';


export default class IndecisionApp extends React.Component{
    state = { 
        options: [], 
        selectedOption: undefined
    }

    handleSelectedOptionReset = () => {
        this.setState(() => ({selectedOption : undefined}));
    }
   
    handleDeleteOption = (optionToRemove) => {
        this.setState((prevState) => ({ options : prevState.options.filter((option  => option !== optionToRemove))}))
    };

    handleAddOption = (option) => {
        if(!option) {
            return 'Enter valid value to add item';
        } else if (this.state.options.indexOf(option) > -1) {
            return 'This option already exists';
        }
            this.setState((previousState) => ({ options : [...previousState.options, option] }));
    };

    handleRemoveAll = () => {
        this.setState(() => ({ options : [] }));
    };

    onMakeDecision = (e) => {
        console.log("make decision");
        const options = this.state.options;
        const max = options.length;
        const randomNum = Math.floor(Math.random() * max);
        const option = options[randomNum];
        this.setState(() => ({ selectedOption: option}));
    };

    componentDidMount() {
        try {    
            const json = localStorage.getItem('options');
            
            if(json) {
                const options = JSON.parse(json);
                this.setState(() => ({options}));
            }

        } catch (e) {
            // do nothing at all
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }

    render() {
        const subtitle = "Put your life in the hands of a computer.";
        const options = this.state.options;

       return (
        <div>
            <Header subtitle={subtitle} />
            <div className="container">
                <Action disabled={options.length > 0} action={this.onMakeDecision}/>
                <div className="widget">
                    <Options options={options} handleDeleteOption={this.handleDeleteOption} handleDeleteOptions={this.handleRemoveAll}/>
                    <AddOption action={this.handleAddOption}/>
                </div>
                <OptionModal selectedOption={this.state.selectedOption} handleReset={this.handleSelectedOptionReset}/>
            </div>
        </div>
       ) 
    }
}